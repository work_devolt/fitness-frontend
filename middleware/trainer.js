export default function ({ $auth, redirect, route }) {

    // if (!roles.includes($auth.user.attributes.role)) {




    if ($auth.user && $auth.user.primary_role.name === 'trainer') {

        let allowedTrainerUrls = [
            '/trainers/' + $auth.user.trainer_id,
            '/'
        ]

        console.info('Path is ' + route.path)

        if(!allowedTrainerUrls.includes(route.path)) {
            console.info('Auth role is Trainer, redirecting to /trainers/' + $auth.user.id)
            return  redirect('/trainers/' + $auth.user.trainer_id,)

        }

        // console.info('Path is ' + route.path)

        //

        // error({ statusCode: 403, message: 'Вам сюда нельзя' })
    }
}
